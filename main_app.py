"""
v0.1
Small program to get card count from https://www.legends-decks.com/ decks.
Requirements:
    - beautifulsoup4==4.5.3
Usage:
    1) Have 'decks.txt' in the same folder as the .py executable and have one deck link per line
    2) Run script 'main_app.py' with 'python main_app.py' in folder
"""

from urllib import request
from collections import Counter
from os import path

from bs4 import BeautifulSoup


def getCardsFromLinkedDeck(url):
    """
    Enter a URL to a legends-decks.com ESL deck and the function will return a Counter with card name (key) and amount
    """
    page_content = request.urlopen(url)

    soup = BeautifulSoup(page_content, 'html.parser')

    res = Counter()

    for link in soup.findAll('span', {'class' : 'gradient'}):
        name = link.find('span', {'class' : 'name'})
        amount = link.find('span', {'class' : 'number'})
        res[name.text.strip()] = int(amount.text.strip())

    return res

def getCardsFromLinkList():
    """
    Reads decks.txt from root (one deck per line) and returns a Counter with all cards and their count
    """

    res = Counter()
    file_obj = None

    try:
        file_obj = open(path.join(path.dirname(__file__), 'decks.txt'), 'r')
    except IOError:
        print('decks.txt File not found!')
    if file_obj:
        for line in file_obj:
            res += getCardsFromLinkedDeck(line)
    return res

def pprintCounter(counter):
    """
    Pretty print a Counter with the format Card : amount
    """
    for item in Counter(counter).most_common():
        print("{}x {}".format(item[1], item[0]))

pprintCounter(getCardsFromLinkList())
