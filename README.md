v0.1 (2017/15/03)

Small program to get card count from https://www.legends-decks.com/ decks.
Requirements:

* beautifulsoup4==4.5.3


Usage:

1. Have 'decks.txt' in the same folder as the .py executable and have one deck link per line

2. Run script 'main_app.py' with 'python main_app.py' in folder